# Tarea_sem_3

## Renderización de listas

### v-for

Usamos esta directiva para iterar elementos de un arreglo. La sintaxis consiste en colocar "i in arr" donde i será el iterador, es decir la variable que irá tomando los distintos valores del arreglo arr.

### Range v for

Por otro lado esta directiva también puede simplemente recibir un número entero que ocasionará que la instrucción se repita tantas veces como indique dicho número.
Por ejemplo:

< ul class="list-group" class list-group-item> 

    < li v-for="i in 9">
        {{i+1}}
    < /li >
    
< /ul >

Este ejemplo nos dice que se mostrarán en pantalla los números consecutivos desde el 1 hasta el 10. Tomar en cuenta que i toma valores desde 0, por ello se coloca i+1.


### Iterar a traves de un arreglo

Cuando se quiera iterar un arreglo de cadenas de texto se debe primero dar un nombre al arreglo y luego colocar sus elementos entre comillas y separados por comas.

Ejemplo:

data:{

    frutas: [
        "manzana",
        "pera",
        "naranja"
    ]
    
}


< li v-for="i in frutas" class="list-group-item" >

    - {{frutas}}
    
< /li >

En este caso i tomará el valor de cada fruta hasta mostrar todas en pantalla.


### Iterar a través de un arreglo de objetos

En caso queramos colocar para cada elemento del arreglo dos o más atributos, utilizaremos:

frutas: [ 

    {
        nombre: "pera",
        precio: 8
    },
    {
        nombre: "naranja",
        precio: 3
    }
    
]

Mientras que para mostrar en pantalla lo indicado será necesario utilizar un punto entre el iterador y el atributo.

< li v-for="(i, index) in frutas" class="list-group-item">

    {{index + 1}} Fruta: {{i.nombre}}  Precio: {{iprecio}}
    
< /li >

Observación: El index sirve como índice para enumerar la lista de frutas y empieza por defecto en 0. Dicho índice no necesita un arreglo o entero que le indique qué valores tomará sino que directamente asume valores de 0 hasta terminar la lista.

Finalmente, si se requieren colocar muchos más atributos como los 5 solicitados en la presente tarea, sería más cómodo usar key y value. Key ayudaría a mostrar el nombre del atributo y value, el valor que toma este.

Ejemplo:

data: {

    fruta:{
        nombre: "pera",
        precio: "3",
        peso: "1kg"
    }
    
}

Luego key, value y opcionalmente index se colocan dentro del paréntesis como iteradores.

< li v-for = "(value, key, index) in fruta" class="list-group-item" >

    {{index}} : {{key}} : {{value}}

< /li >